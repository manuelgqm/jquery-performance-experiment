context('Main', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080/')
  })


  it('location', () => {
    cy.location().should( location => {
      expect(location.href).to.eq('http://localhost:8080/')
    })
  })

})