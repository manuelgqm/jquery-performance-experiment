import $ from 'jquery';
import products from './products';
import template from './template'

const INITIAL_TIME = performance.now()

$("body").append(template)
$(".title").html("Optimized JQuery Products Listing")

let products_li = ''
products.forEach( item => {
  let discount_span = item.discount != 0 ? 
    `<span class="item-prop">- ${item.discount}% discount</span>` :
    ''

  let available_span = item.available ?
    `<span class="item-prop item-available">available</span>` :
    `<span class="item-prop item-out-of-stock">out of stock</span>`
  
  products_li += `
    <li>
      <span class="item-prop item-name">${item.name}</span>
      <span class="item-prop">${item.price}€</span>
      ${discount_span}
      ${available_span}
    </li>
  `
})

$("#products").append(products_li)

const run_time = performance.now() - INITIAL_TIME

$("#performance").append(`${Math.round(run_time)} miliseconds`)
