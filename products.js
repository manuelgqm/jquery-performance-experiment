const MAX_PRODUCTS = 500;

let products = [
  {
    name: 'Red stripped coat',
    price: 80,
    discount: 5,
    available: true
  }, {
    name: 'Leopard skin pants',
    price: 45,
    discount: 0,
    available: true
  }, {
    name: 'Tony Hawk shoes',
    price: 50,
    discount: 10,  
    available: false
}]

for (let i = 0; i < MAX_PRODUCTS; i++ ){
  products.push({
    name: 'Funny yellow tie',
    price: 20,
    discount: 3,
    available: true
  })
}

export default products