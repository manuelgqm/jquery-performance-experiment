# Example of JQuery to Vue.js migration

## Description
This repo exposes a basic example of a product page with performance issues in JQuery. Also there is a better implementation making use of Vue.js framework solving the performance problems.
It makes use of vue-cli for fast startup with prototyping feature

## Dependencies
- node
- cypress
- vue-cli

## Startup
In command prompt run:
`npm install -g @vue/cli`
`npm install -g @vue/cli-service-global`

In project folder run:
`npm install cypress --save-dev`
`npm install jquery --save-dev`

## Running
- To serve in http://localnost:8080 run: `vue serve`
- To run cypress tests in ui mode run: `npx cypres open`