export default () => `
  <style>
    li {
      list-style-type: none;
      font-size: 14pt;
      margin: 15px;
    }
    .item-prop { margin-right: 10px}
    .item-name {
      font-weight: bold;
    }
    .item-available {
      font-weight: bold;
      color: green;
    }
    .item-out-of-stock{
      font-weight: bold;
      color: red;
    }
  </style>
  <h1 class="title"></h1>
  <h2 id="performance">
    Performance:
  </h2>
  <div id="products"></div>
`