import $ from 'jquery';
import products from './products';
import template from './template'
const INITIAL_TIME = performance.now()

$("body").append(template)
$(".title").html("JQuery Products Listing")

products.forEach( item => {
  $("#products").append('<li></li>').round
  $("#products li:last").append(`<span class="item-prop item-name">${item.name}:</span>`)
  $("#products li:last").append(`<span class="item-prop">${item.price}€</span>`)
  if (item.discount != 0) {
    $("#products li:last").append(`<span class="item-prop">- ${item.discount}% discount</span>`)
  }
  if (item.available) {
    $("#products li:last").append(`<span class="item-prop item-available">available</span>`)
  } else {
    $("#products li:last").append(`<span class="item-prop item-out-of-stock">out of stock</span>`)
  }
})

const run_time = performance.now() - INITIAL_TIME

$("#performance").append(`${Math.round(run_time)} miliseconds`)
